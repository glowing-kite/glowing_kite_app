
/**
 * @brief Fades the 2 LEDs for the duration in sekonds. Max brightness gives some notion of the brighness after the fade duration.
 *        After the duration the buzzer is activated.
 * 
 * @param max_brightness Some notion of brightness. Max value is 500000.
 * @param fade_duration_sek Duration of fading in sekonds
 * @return uint32_t returns 0
 */
uint32_t fade_pwm(uint32_t max_brightness, uint32_t fade_duration_sek);

/**
 * @brief Stops the LEDS and the buzzer.
 * 
 * @return int returns 0;
 */
int stop_fade(void);