#include "gk_rtc.h"

#include <device.h>
#include <drivers/counter.h>
#include <sys/timeutil.h>

#include <stm32_ll_rcc.h>
#include <stm32_ll_rtc.h>

#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>

struct rtc_stm32_data {
    counter_alarm_callback_t callback_a;
    counter_alarm_callback_t callback_b;
    uint32_t ticks;
    void* user_data;
};

/**
 * @brief Time offsets of MEZ and MESZ in hours.
 * 
 */
enum time_offset_t {
    NOT_INIT = 0,
    WINTER_TIME = 1,
    SUMMER_TIME = 2
};

// Holds a pointer to the RTC device so we need to only find it once
static const struct device* rtc_device_global;
/**
 * Holds the current time offset depending on MEZ/MESZ.
 * The time offset is also saved in a backup register of the RTC to make sure we know
 * in what timezone the RTC clock is currently represented. This variable makes sure the current time_offset is
 * initialized before we give the UTC time to someone.
 */
static enum time_offset_t time_offset = NOT_INIT;

static void daylight_saving_callback(const struct device* dev, uint8_t id, uint32_t syncclock, void* ud);

static const struct device* get_rtc_device(void)
{
    if (rtc_device_global) {
        return rtc_device_global;
    }
    rtc_device_global = device_get_binding("RTC_0");
    if (!rtc_device_global) {
        printk("No device RTC_0 available\n");
        return NULL;
    }
    return rtc_device_global;
}

struct tm get_rtc_tm_time(void)
{
    struct tm now = { 0 };
    uint32_t rtc_date, rtc_time;

    /* Read time and date registers */
    rtc_time = LL_RTC_TIME_Get(RTC);
    rtc_date = LL_RTC_DATE_Get(RTC);

    // years from 1900
    // get the 2 most significant digits from backup registers
    now.tm_year = LL_RTC_BAK_GetRegister(RTC, LL_RTC_BKP_DR3) + __LL_RTC_CONVERT_BCD2BIN(__LL_RTC_GET_YEAR(rtc_date));

    /* tm_mon allowed values are 0-11 */
    now.tm_mon = __LL_RTC_CONVERT_BCD2BIN(__LL_RTC_GET_MONTH(rtc_date)) - 1;
    now.tm_mday = __LL_RTC_CONVERT_BCD2BIN(__LL_RTC_GET_DAY(rtc_date));

    // weekdays are 1-7 with Monday 1 and Sunday 7
    // convert sunday to 0
    now.tm_wday = __LL_RTC_GET_WEEKDAY(rtc_date) % 7;

    now.tm_hour = __LL_RTC_CONVERT_BCD2BIN(__LL_RTC_GET_HOUR(rtc_time));
    now.tm_min = __LL_RTC_CONVERT_BCD2BIN(__LL_RTC_GET_MINUTE(rtc_time));
    now.tm_sec = __LL_RTC_CONVERT_BCD2BIN(__LL_RTC_GET_SECOND(rtc_time));

    return now;
}

time_t get_rtc_time_t_utc(void)
{
    while (!time_offset) {
        k_sleep(K_MSEC(10));
    }
    struct tm now = get_rtc_tm_time();
    time_t ts = timeutil_timegm(&now);

    return ts - 3600 * time_offset;
}

int set_rtc_time(struct tm set_time, bool adjust_daylight_saving)
{
    LL_RTC_DateTypeDef RTC_DateStruct;
    LL_RTC_DATE_StructInit(&RTC_DateStruct);

    // Conversions from struct tm to RTC representations of months etc (1-12 vs. 0-11)
    RTC_DateStruct.Month = set_time.tm_mon + 1;
    RTC_DateStruct.Day = set_time.tm_mday;
    RTC_DateStruct.WeekDay = set_time.tm_wday ? set_time.tm_wday : 7;

    // The RTC can only save the last 2 digits of the year.
    // Save the others in a backup register
    RTC_DateStruct.Year = set_time.tm_year % 100;
    LL_RTC_BAK_SetRegister(RTC, LL_RTC_BKP_DR3, set_time.tm_year - RTC_DateStruct.Year);

    LL_RTC_TimeTypeDef RTC_TimeStruct;
    LL_RTC_TIME_StructInit(&RTC_TimeStruct);

    RTC_TimeStruct.Hours = set_time.tm_hour;
    RTC_TimeStruct.Minutes = set_time.tm_min;
    RTC_TimeStruct.Seconds = set_time.tm_sec;
    RTC_TimeStruct.TimeFormat = LL_RTC_TIME_FORMAT_AM_OR_24;

    if (ERROR == LL_RTC_TIME_Init(RTC, LL_RTC_FORMAT_BIN, &RTC_TimeStruct)) {
        printf("couldn't configure time\n");
    }
    if (ERROR == LL_RTC_DATE_Init(RTC, LL_RTC_FORMAT_BIN, &RTC_DateStruct)) {
        printf("couldn't configure time\n");
    }

    if (adjust_daylight_saving) {
        LL_RTC_BAK_SetRegister(RTC, LL_RTC_BKP_DR0, NOT_INIT);
        handle_daylight_saving();
    }

    return 0;
}

int set_rtc_alarm(stm32_alarm alarm)
{
    const struct device* dev = get_rtc_device();
    LL_RTC_AlarmTypeDef rtc_alarm;
    struct rtc_stm32_data* data = ((struct rtc_stm32_data*)(dev)->data);

    LL_RTC_DisableWriteProtection(RTC);
    LL_RTC_ALMA_Disable(RTC);
    LL_RTC_EnableWriteProtection(RTC);

    data->callback_a = alarm.callback;
    data->user_data = NULL;

    rtc_alarm.AlarmTime.TimeFormat = LL_RTC_TIME_FORMAT_AM_OR_24;
    rtc_alarm.AlarmTime.Hours = alarm.hour;
    rtc_alarm.AlarmTime.Minutes = alarm.minute;
    rtc_alarm.AlarmTime.Seconds = 0;

    rtc_alarm.AlarmMask = LL_RTC_ALMB_MASK_DATEWEEKDAY;

    if (LL_RTC_ALMA_Init(RTC, LL_RTC_FORMAT_BIN, &rtc_alarm) != SUCCESS) {
        printf("couldn't set alarm\n");
        return -EIO;
    }

    LL_RTC_DisableWriteProtection(RTC);
    LL_RTC_ALMA_Enable(RTC);
    LL_RTC_ClearFlag_ALRA(RTC);
    LL_RTC_EnableIT_ALRA(RTC);
    LL_RTC_EnableWriteProtection(RTC);

    return 0;
}

int get_rtc_alarm(stm32_alarm* alarm)
{

    uint32_t current_alarm_hour = __LL_RTC_CONVERT_BCD2BIN(LL_RTC_ALMA_GetHour(RTC));
    uint32_t current_alarm_minute = __LL_RTC_CONVERT_BCD2BIN(LL_RTC_ALMA_GetMinute(RTC));

    alarm->hour = current_alarm_hour;
    alarm->minute = current_alarm_minute;
    return 0;
}

/**
 * @brief Returns true if the provided date is in MESZ and false otherwise.
 *        The days where the switch happens belong to the period where the switch happens to.
 *        For example the last Sunday in March is allways in MESZ regardless of time of day.
 * 
 * @param now Day that daylighsaving time is calculated for.
 * @return bool True for MESZ and false for MEZ. 
 */
static bool is_dst(struct tm now)
{
    if (now.tm_mon < 2 || now.tm_mon > 9)
        return false;
    if (now.tm_mon > 2 && now.tm_mon < 9)
        return true;

    /**
     * The latest the last Sunday in a month can be is day
     * 31-6 = 25 since if the 24st is a Sunday so will be the 31st
     * It follows if the month day of the last Sunday is bigger than 25
     * it's the last Sunday of the month.
     */
    int previousSunday = now.tm_mday - now.tm_wday;

    if (now.tm_mon == 2)
        return previousSunday >= 25;
    if (now.tm_mon == 9)
        return previousSunday < 25;

    return false; // this line never gonna happend
}

/**
 * @brief Returns the next time a daylightsaving event happens from the provided date on. 
 * 
 * @param now Date to start from
 * @return struct tm Date of next daylightsaving event
 */
static struct tm get_next_daylight_saving_event(struct tm now)
{
    /**
     * The daylightsaving switch happens at the last Sunday in March/Oktober at 2:00/3:00.
     * We first figure out what week day the 31st of the respective month is and use that
     * to figure out what date the last Sunday is.
     */
    struct tm next_day_light_saving_event = { 0 };
    next_day_light_saving_event.tm_mday = 31;
    next_day_light_saving_event.tm_year = now.tm_year;

    if (is_dst(now)) {
        next_day_light_saving_event.tm_mon = 9;
        next_day_light_saving_event.tm_hour = 3;
    } else {
        next_day_light_saving_event.tm_mon = 2;
        next_day_light_saving_event.tm_hour = 2;
    }

    time_t t = mktime(&next_day_light_saving_event);
    // Substract weekday of the 31st to get the date of the last Sunday
    // tm_wday is days since sunday
    next_day_light_saving_event.tm_mday -= localtime(&t)->tm_wday;
    next_day_light_saving_event.tm_wday = 0;
    return next_day_light_saving_event;
}

uint32_t set_alarm_for_daylight_saving(struct tm next_day_light_saving_event)
{
    LL_RTC_AlarmTypeDef alarm = { 0 };
    alarm.AlarmMask = LL_RTC_ALMB_MASK_NONE;
    alarm.AlarmDateWeekDaySel = LL_RTC_ALMB_DATEWEEKDAYSEL_DATE;
    alarm.AlarmDateWeekDay = next_day_light_saving_event.tm_mday;

    alarm.AlarmTime.TimeFormat = LL_RTC_TIME_FORMAT_AM_OR_24;
    alarm.AlarmTime.Hours = next_day_light_saving_event.tm_hour;
    alarm.AlarmTime.Minutes = 0;

    const struct device* dev = get_rtc_device();
    struct rtc_stm32_data* data = ((struct rtc_stm32_data*)(dev)->data);

    LL_RTC_DisableWriteProtection(RTC);
    LL_RTC_ALMB_Disable(RTC);
    LL_RTC_EnableWriteProtection(RTC);

    data->callback_b = daylight_saving_callback;
    data->user_data = NULL;

    if (LL_RTC_ALMB_Init(RTC, LL_RTC_FORMAT_BIN, &alarm) != SUCCESS) {
        printf("couldn't set alarm B\n");
        return -EIO;
    }

    LL_RTC_DisableWriteProtection(RTC);
    LL_RTC_ALMB_Enable(RTC);
    LL_RTC_ClearFlag_ALRB(RTC);
    LL_RTC_EnableIT_ALRB(RTC);
    LL_RTC_EnableWriteProtection(RTC);
    return 0;
}

static void daylight_saving_callback(const struct device* dev,
    uint8_t id,
    uint32_t syncclock,
    void* ud)
{
    handle_daylight_saving();
}

uint32_t handle_daylight_saving(void)
{
    struct tm now = get_rtc_tm_time();
    enum time_offset_t time_offset_saved = LL_RTC_BAK_GetRegister(RTC, LL_RTC_BKP_DR0);
    if (is_dst(now)) {
        time_offset = SUMMER_TIME;
        LL_RTC_BAK_SetRegister(RTC, LL_RTC_BKP_DR0, SUMMER_TIME);
        if (time_offset_saved == WINTER_TIME) {
            now.tm_hour += 1;
            set_rtc_time(now, false);
        } else if (time_offset_saved == NOT_INIT) {
            now.tm_hour += 2;
            set_rtc_time(now, false);
        }
    } else {
        LL_RTC_BAK_SetRegister(RTC, LL_RTC_BKP_DR0, WINTER_TIME);
        time_offset = WINTER_TIME;
        if (time_offset_saved == SUMMER_TIME) {
            now.tm_hour -= 1;
            set_rtc_time(now, false);
        } else if (time_offset_saved == NOT_INIT) {
            now.tm_hour += 1;
            set_rtc_time(now, false);
        }
    }

    struct tm next_day_light_saving_event = get_next_daylight_saving_event(now);
    printf("next daylight saving event: %u.%u.%u %u:%u Uhr\n",
        next_day_light_saving_event.tm_mday,
        next_day_light_saving_event.tm_mon + 1,
        next_day_light_saving_event.tm_year + 1900,
        next_day_light_saving_event.tm_hour,
        next_day_light_saving_event.tm_min);

    set_alarm_for_daylight_saving(next_day_light_saving_event);

    return 0;
}

uint32_t save_in_bkp(uint32_t addr, uint32_t data)
{
    switch (addr) {
    case 0x1:
        LL_RTC_BAK_SetRegister(RTC, LL_RTC_BKP_DR1, data);
        break;
    case 0x2:
        LL_RTC_BAK_SetRegister(RTC, LL_RTC_BKP_DR2, data);
        break;
    default:
        return (uint32_t)-1;
    }
    return 0;
}

uint32_t get_from_bkp(uint32_t addr, uint32_t* data)
{
    switch (addr) {
    case 0x1:
        *data = LL_RTC_BAK_GetRegister(RTC, LL_RTC_BKP_DR1);
        break;
    case 0x2:
        *data = LL_RTC_BAK_GetRegister(RTC, LL_RTC_BKP_DR2);
        break;
    default:
        return (uint32_t)-1;
    }
    return 0;
}