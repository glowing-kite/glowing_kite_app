#include <stdint.h>

/**
 * @brief Gets the number of the button that was pressed.
 * 
 */
typedef void (*button_callback_t)(uint32_t);

/**
 * @brief Initialized GPIOs and registers function that handles button presses.
 * 
 * @param button_callback Function that handles button presses.
 */
void init_buttons(button_callback_t button_callback);