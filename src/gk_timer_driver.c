#include "gk_timer_driver.h"

#include <device.h>
#include <kernel.h>
#include <logging/log.h>

#include <drivers/clock_control/stm32_clock_control.h>
#include <drivers/pwm.h>

#include <stm32_ll_rcc.h>
#include <stm32_ll_tim.h>

#include <errno.h>
#include <math.h>
#include <stdio.h>

LOG_MODULE_REGISTER(gk_timer, LOG_LEVEL_DBG);

#define DT_DRV_COMPAT gk_timer

struct gk_timer_config {
    /** Timer instance. */
    TIM_TypeDef* timer;
    /** Prescaler. */
    uint32_t prescaler;
    /** Clock configuration. */
    struct stm32_pclken pclken;
};

struct gk_timer_data {
    /** Timer clock (Hz). */
    uint32_t tim_clk;
};

// We have global objects because we want to use a direct isr and I'm not sure if
// we can access a device struct from there
static gk_timer_callback_t gk_timer_callback = NULL;
static const struct device* gk_timer_device = NULL;

/**
 * Obtain timer clock speed.
 *
 * @param pclken  Timer clock control subsystem.
 * @param tim_clk Where computed timer clock will be stored.
 *
 * @return 0 on success, error code otherwise.
 */
static int get_tim_clk(const struct stm32_pclken* pclken, uint32_t* tim_clk)
{
    int r;
    const struct device* clk;
    uint32_t bus_clk, apb_psc;

    clk = device_get_binding(STM32_CLOCK_CONTROL_NAME);
    __ASSERT_NO_MSG(clk);

    r = clock_control_get_rate(clk, (clock_control_subsys_t*)pclken,
        &bus_clk);
    if (r < 0) {
        return r;
    }

    if (pclken->bus == STM32_CLOCK_BUS_APB1) {
        apb_psc = CONFIG_CLOCK_STM32_APB1_PRESCALER;
    } else {
        apb_psc = CONFIG_CLOCK_STM32_APB2_PRESCALER;
    }

    /*
	 * If the APB prescaler equals 1, the timer clock frequencies
	 * are set to the same frequency as that of the APB domain.
	 * Otherwise, they are set to twice (×2) the frequency of the
	 * APB domain.
	 */
    if (apb_psc == 1u) {
        *tim_clk = bus_clk;
    } else {
        *tim_clk = bus_clk * 2u;
    }

    return 0;
}


ISR_DIRECT_DECLARE(timer_irq_handler)
{
    const struct gk_timer_config* cfg = gk_timer_device->config;
    LL_TIM_ClearFlag_UPDATE(cfg->timer);
    if (gk_timer_callback) {
        gk_timer_callback();
    } else {
    }
    return 0;
}

static int timer_stm32_init(const struct device* dev)
{
    struct gk_timer_data* data = dev->data;
    const struct gk_timer_config* cfg = dev->config;

    int r;
    const struct device* clk;

    /* enable clock and store its speed */
    clk = device_get_binding(STM32_CLOCK_CONTROL_NAME);
    __ASSERT_NO_MSG(clk);

    r = clock_control_on(clk, (clock_control_subsys_t*)&cfg->pclken);
    if (r < 0) {
        LOG_ERR("Could not initialize clock (%d)", r);
        return r;
    }
    r = get_tim_clk(&cfg->pclken, &data->tim_clk);
    if (r < 0) {
        LOG_ERR("Could not obtain timer clock (%d)", r);
        return r;
    }

    IRQ_DIRECT_CONNECT(DT_IRQN(DT_NODELABEL(timers4)),
        DT_IRQ(DT_NODELABEL(timers4), priority),
        timer_irq_handler, 0);

    irq_enable(DT_IRQN(DT_NODELABEL(timers4)));

    gk_timer_device = dev;

    return 0;
}

void register_gk_timer_callback(gk_timer_callback_t callback)
{
    gk_timer_callback = callback;
}

void disable_gk_timer(void)
{
    const struct gk_timer_config* cfg = gk_timer_device->config;
    LL_TIM_DisableCounter(cfg->timer);
}

int init_timer(LL_TIM_InitTypeDef init)
{

    const struct gk_timer_config* cfg = gk_timer_device->config;

    if (LL_TIM_Init(cfg->timer, &init) != SUCCESS) {
        LOG_ERR("Could not initialize timer");
        return -EIO;
    }

    LL_TIM_ClearFlag_UPDATE(cfg->timer);
    LL_TIM_EnableIT_UPDATE(cfg->timer);

    LL_TIM_EnableCounter(cfg->timer);
    return 0;
}

static struct gk_timer_data gk_timer_data = { 0 };
static const struct gk_timer_config gk_timer_config = {
    .timer = (TIM_TypeDef*)DT_REG_ADDR(DT_PARENT(DT_DRV_INST(0))),
    .pclken = {
        .enr = DT_CLOCKS_CELL(DT_PARENT(DT_DRV_INST(0)), bits),
        .bus = DT_CLOCKS_CELL(DT_PARENT(DT_DRV_INST(0)), bus),
    },
};

DEVICE_DT_INST_DEFINE(0, &timer_stm32_init, device_pm_control_nop,
    &gk_timer_data,
    &gk_timer_config, POST_KERNEL,
    CONFIG_KERNEL_INIT_PRIORITY_DEVICE,
    NULL);