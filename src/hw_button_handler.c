#include <drivers/gpio.h>
#include <stdio.h>

#include "hw_button_handler.h"

#define BUTTON_GPIO_PORT_LABEL DT_GPIO_LABEL_BY_IDX(DT_NODELABEL(button0), gpios, 0)
#define BUTTON_0_PIN DT_GPIO_PIN(DT_NODELABEL(button0), gpios)
#define BUTTON_1_PIN DT_GPIO_PIN(DT_NODELABEL(button1), gpios)
#define BUTTON_2_PIN DT_GPIO_PIN(DT_NODELABEL(button2), gpios)
#define BUTTON_3_PIN DT_GPIO_PIN(DT_NODELABEL(button3), gpios)
#define BUTTON_CONFIG (GPIO_INPUT | GPIO_PULL_UP | DT_GPIO_FLAGS(DT_NODELABEL(button0), gpios))
#define BUTTON_COUNT 4

// Needed by zephyr callback init. Not used.
static struct gpio_callback button_cb_data;
// Holds the state of each button. Is updated in the isr.
static uint32_t button_states[BUTTON_COUNT] = { 0 };
// Holds the pin number associated with each button.
static uint32_t button_pins[BUTTON_COUNT] = { 0 };
// Holds a pointer to the callback function of the user.
// Is called once a button is pressed.
static button_callback_t global_button_callback = 0;
// Make sure the button_ressed routine is only run once at a time
K_MUTEX_DEFINE(button_pressed_mut);

static void button_pressed(const struct device* dev, struct gpio_callback* cb, uint32_t pins)
{
    uint32_t press_counter[9] = { 0 };
    uint32_t read_val = 0;
    if (k_mutex_lock(&button_pressed_mut, K_NO_WAIT)) {
        return;
    }

    /**
     * Button debouncing:
     * Count how often in 600 loops the value of all buttons is different from the saved state of the button.
     * Callback is only called if the number is larger than 500.
     */
    for (uint32_t i = 0; i < 600; ++i) {
        gpio_port_get_raw(dev, &read_val);
        for (uint32_t j = 0; j < BUTTON_COUNT; ++j) {
            uint32_t pin_level = (read_val & ((1 << button_pins[j]))) >> button_pins[j];
            if (pin_level != button_states[j]) {
                press_counter[j] += 1;
            }
        }
    }

    for (uint32_t j = 0; j < BUTTON_COUNT; ++j) {
        if (press_counter[j] > 500) {
            button_states[j] = !button_states[j];
            if (button_states[j] == 0) {
                if (global_button_callback == 0) {
                    printk("no button_callback registered\n");
                } else {
                    global_button_callback(j);
                }
            }
            printk("Button %d val: %" PRIu32 "\n", j, button_states[j]);
        }
    }
    k_mutex_unlock(&button_pressed_mut);
}

void init_buttons(button_callback_t button_callback)
{
    const struct device* button_gpio_port;
    int ret;
    uint32_t callback_mask = 0;

    button_pins[0] = BUTTON_0_PIN;
    button_pins[1] = BUTTON_1_PIN;
    button_pins[2] = BUTTON_2_PIN;
    button_pins[3] = BUTTON_3_PIN;

    button_gpio_port = device_get_binding(BUTTON_GPIO_PORT_LABEL);
    if (button_gpio_port == NULL) {
        printk("Error: didn't find %s device\n", BUTTON_GPIO_PORT_LABEL);
        return;
    }

    for (uint32_t i = 0; i < BUTTON_COUNT; ++i) {
        button_states[i] = 1;
        ret = gpio_pin_configure(button_gpio_port, button_pins[i], BUTTON_CONFIG);
        if (ret != 0) {
            printk("Error %d: failed to configure %s pin %d\n",
                ret, BUTTON_GPIO_PORT_LABEL, button_pins[i]);
            return;
        }

        ret = gpio_pin_interrupt_configure(button_gpio_port,
            button_pins[i],
            GPIO_INT_EDGE_BOTH);
        if (ret != 0) {
            printk("Error %d: failed to configure interrupt on %s pin %d\n",
                ret, BUTTON_GPIO_PORT_LABEL, button_pins[i]);
            return;
        }
        callback_mask |= BIT(button_pins[i]);
    }

    global_button_callback = button_callback;
    gpio_init_callback(&button_cb_data, button_pressed, callback_mask);
    gpio_add_callback(button_gpio_port, &button_cb_data);
}