#include "gk_timer_driver.h"

#include <device.h>
#include <kernel.h>
#include <logging/log.h>

#include <drivers/clock_control/stm32_clock_control.h>
#include <drivers/pwm.h>

#include <stm32_ll_rcc.h>
#include <stm32_ll_tim.h>

#include <errno.h>
#include <math.h>
#include <stdio.h>

#define PWM_LED0_NODE DT_ALIAS(pwm_led0)
#define PWM_LED0_LABEL DT_PWMS_LABEL(PWM_LED0_NODE)
#define PWM_LED0_CHANNEL DT_PWMS_CHANNEL(PWM_LED0_NODE)
#define PWM_LED0_FLAGS DT_PWMS_FLAGS(PWM_LED0_NODE)

#define PWM_LED1_NODE DT_ALIAS(pwm_led1)
#define PWM_LED1_LABEL DT_PWMS_LABEL(PWM_LED1_NODE)
#define PWM_LED1_CHANNEL DT_PWMS_CHANNEL(PWM_LED1_NODE)
#define PWM_LED1_FLAGS DT_PWMS_FLAGS(PWM_LED1_NODE)

#define PWM_BUZZER_NODE DT_ALIAS(pwm_buzzer)
#define PWM_BUZZER_LABEL DT_PWMS_LABEL(PWM_BUZZER_NODE)
#define PWM_BUZZER_CHANNEL DT_PWMS_CHANNEL(PWM_BUZZER_NODE)
#define PWM_BUZZER_FLAGS DT_PWMS_FLAGS(PWM_BUZZER_NODE)

int stop_fade(void);
void automatic_turn_off(struct k_timer *timer_id) {
    stop_fade();
}
K_TIMER_DEFINE(automatic_turn_off_timer, automatic_turn_off, NULL);


static const struct device* pwm0_device;
static const struct device* pwm1_device;
static const struct device* pwm_buzzer_device;


// For explanation look atfade_pwm_exp
static uint32_t glob_max_brightness = 0;
static const uint32_t pulse_offset = 23000;
static double exp_offset = -14.0;
static double global_brightness_step = 0.0;
static double exp_count = 0.0;

static void timer_irq_handler(void)
{
    const uint32_t period = 1000000;
    static uint32_t pulse_width = 0;

    pulse_width = pulse_offset + exp(exp_offset + exp_count) * glob_max_brightness;
    exp_count += global_brightness_step;

    if (pulse_width < glob_max_brightness) {
        pwm_pin_set_nsec(pwm0_device, PWM_LED0_CHANNEL, period, pulse_width, PWM_LED0_FLAGS);
        pwm_pin_set_nsec(pwm1_device, PWM_LED1_CHANNEL, period, pulse_width, PWM_LED0_FLAGS);
    } else {
        pwm_pin_set_nsec(pwm_buzzer_device, PWM_BUZZER_CHANNEL, 40000, 40000 / 2, PWM_LED0_FLAGS);
        disable_gk_timer();
        pulse_width = 0;
        exp_count = 0.0;
        k_timer_start(&automatic_turn_off_timer, K_SECONDS(10), K_FOREVER);
    }
}

static int fade_pwm_exp(uint32_t max_brightness, uint32_t fade_duration_sek)
{
    const uint32_t start_period = 23000;
    uint32_t fade_duration_ms = fade_duration_sek * 1000;
    uint32_t number_of_timer_irqs = fade_duration_ms;
    exp_count = 0.0;

	/**
	 * The fading works by having timer interrupts that occur every ms and adjusting the pwm pulse_width for the LEDs.
	 * The pulse_width updated by:
	 * 23000 + exp(-14 + x)*max_brigness
	 * LEDs don't increase their brightness linearly with increasing pwm pulse width thats why we use the exp function.
	 * x is increased in every interrupt by 14/number_of_irqs. This makes the last interrupt set x to 14 and exp(0) will be 1.
	 * Since we substract 23000 from the max_brightness value here the last value for the pwm pulse width will be max brighness.
	 * 23000 is an experimental value for the pwm pulse_width value at which the LEDs start lighting up.
	 * The -14 offset in the exp function is experimental aswell and is exp_offset in the code.
	 */

    global_brightness_step = fabs(exp_offset) / number_of_timer_irqs;
    glob_max_brightness = max_brightness - start_period;

    register_gk_timer_callback(&timer_irq_handler);
    LL_TIM_InitTypeDef init;

    /* initialize timer */
    LL_TIM_StructInit(&init);

	/**
	 * 50000000/(4+1) = 10000000 Hz clock
	 * 10000000Hz/10000 = 1000Hz => irq every ms
	 */
    init.Prescaler = 4;
    init.CounterMode = LL_TIM_COUNTERMODE_UP;
    init.Autoreload = 10000;
    init.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;

    init_timer(init);

    return 0;
}

uint32_t fade_pwm(uint32_t max_brightness, uint32_t fade_duration_sek)
{
    pwm0_device = device_get_binding(PWM_LED0_LABEL);
    pwm1_device = device_get_binding(PWM_LED1_LABEL);
    pwm_buzzer_device = device_get_binding(PWM_BUZZER_LABEL);
    if ((pwm0_device == 0) || (pwm1_device == 0) || (pwm_buzzer_device == 0)) {
        printf("Couldn't get pwm devices\n");
        printf("pwm0_device: %p\n", pwm0_device);
        printf("pwm1_device: %p\n", pwm1_device);
        printf("pwm_buzzer_device: %p\n", pwm_buzzer_device);
    } else {
        fade_pwm_exp(max_brightness, fade_duration_sek);
    }

    return 0;
}

int stop_fade(void)
{
    if (pwm0_device && pwm1_device && pwm_buzzer_device) {

        disable_gk_timer();

        pwm_pin_set_nsec(pwm0_device, PWM_LED0_CHANNEL, 0, 0, 0);
        pwm_pin_set_nsec(pwm1_device, PWM_LED1_CHANNEL, 0, 0, 0);
        pwm_pin_set_nsec(pwm_buzzer_device, PWM_BUZZER_CHANNEL, 0, 0, 0);
        exp_count = 0.0;
    } else {
        printf("trying to stop fade without init\n");
    }

    return 0;
}
