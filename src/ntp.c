#include <net/ethernet.h>
#include <net/net_context.h>
#include <net/net_core.h>
#include <net/net_if.h>
#include <net/net_mgmt.h>
#include <net/socket.h>

#include <stdio.h>

#include "gk_rtc.h"

#define ETHERNET_NODE DT_ALIAS(ethernet_controler)
#define ETHERNET_LABLE "eth_spi"

#define NTP_TIMESTAMP_DELTA 2208988800ull

typedef struct
{
    uint8_t li_vn_mode; // Eight bits. li, vn, and mode.
        // li.   Two bits.   Leap indicator.
        // vn.   Three bits. Version number of the protocol.
        // mode. Three bits. Client will pick mode 3 for client.

    uint8_t stratum; // Eight bits. Stratum level of the local clock.
    uint8_t poll; // Eight bits. Maximum interval between successive messages.
    uint8_t precision; // Eight bits. Precision of the local clock.

    uint32_t rootDelay; // 32 bits. Total round trip delay time.
    uint32_t rootDispersion; // 32 bits. Max error aloud from primary clock source.
    uint32_t refId; // 32 bits. Reference clock identifier.

    uint32_t refTm_s; // 32 bits. Reference time-stamp seconds.
    uint32_t refTm_f; // 32 bits. Reference time-stamp fraction of a second.

    uint32_t origTm_s; // 32 bits. Originate time-stamp seconds.
    uint32_t origTm_f; // 32 bits. Originate time-stamp fraction of a second.

    uint32_t rxTm_s; // 32 bits. Received time-stamp seconds.
    uint32_t rxTm_f; // 32 bits. Received time-stamp fraction of a second.

    uint32_t txTm_s; // 32 bits and the most important field the client cares about. Transmit time-stamp seconds.
    uint32_t txTm_f; // 32 bits. Transmit time-stamp fraction of a second.

} ntp_packet; // Total: 384 bits or 48 bytes.

// Callback that gets called from OS once DHCP is done.
static struct net_mgmt_event_callback mgmt_cb;
// Set this increase this semaphore in the DCHP callback so we can wait on it.
K_SEM_DEFINE(dhcp_done_sem, 0, 1);

static void handler(struct net_mgmt_event_callback* cb,
    uint32_t mgmt_event,
    struct net_if* iface)
{
    if (mgmt_event != NET_EVENT_IPV4_ADDR_ADD) {
        return;
    }
    k_sem_give(&dhcp_done_sem);
}

void net_function(void)
{
    struct addrinfo* res;
    int st, sock;
    int n = 0;
    ntp_packet packet = { 0 };
    struct net_if* iface;

    // NTP UDP port number.
    char* portno = "123";
    // NTP server host-name
    char* host_name = "pool.ntp.org";

    const struct device* eth_device = device_get_binding(ETHERNET_LABLE);

    if (!eth_device) {
        printf("Could not get Ethernet device\n");
    }

    // Start DHCP and register callback that notifies us when it's done
    net_mgmt_init_event_callback(&mgmt_cb, handler, NET_EVENT_IPV4_ADDR_ADD);
    net_mgmt_add_event_callback(&mgmt_cb);
    iface = net_if_get_default();
    net_dhcpv4_start(iface);

    // wait till DHCP is done
    if (k_sem_take(&dhcp_done_sem, K_SECONDS(10))) {
        printf("No DHCP connection, turning Ethernet off\n");
        // (((struct ethernet_api*)eth_device->api)->stop)(eth_device);
        int (*stop)(const struct device* dev) = ((struct ethernet_api*)eth_device->api)->stop;
        stop(eth_device);
        return;
    }

    st = getaddrinfo(host_name, portno, NULL, &res);
    if (st != 0) {
        printf("Unable to resolve address, quitting\n");
        return;
    }

    // 2-bit leap-indicator = 0x0
    // 3-bit NTP version number = 0x3
    // 3-bit mode = 0x3 -> client mode
    // packet.li_vn_mode = 0x1b;
    *((char*)&packet + 0) = 0x1b; // Represents 27 in base 10 or 00011011 in base 2.

    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sock < 0) {
        printf("ERROR opening socket\n");
    }

    n = connect(sock, res->ai_addr, res->ai_addrlen);
    if (n < 0) {
        printf("ERROR connecting to socket\n");
    }

    n = send(sock, &packet, sizeof(ntp_packet), 0);
    if (n < 0) {
        printf("ERROR writing to socket\n");
    }

    n = recv(sock, (char*)&packet, sizeof(ntp_packet), 0);
    if (n < 0) {
        printf("ERROR reading from socket\n");
    }

    (void)close(sock);

    // Time-stamp seconds.
    // converts network byte order to host byte order
    packet.txTm_s = ntohl(packet.txTm_s);

    uint64_t RTC_time_local = get_rtc_time_t_utc();
    uint64_t NTP_time = (uint64_t)(packet.txTm_s - NTP_TIMESTAMP_DELTA);

    char time_string_NTP[50] = { 0 };
    char time_string_RTC[50] = { 0 };

    char* tmp = ctime((const time_t*)&NTP_time);
    memcpy(time_string_NTP, tmp, strlen(tmp));

    tmp = ctime((const time_t*)&RTC_time_local);
    memcpy(time_string_RTC, tmp, strlen(tmp));

    printf("time from NTP: %stime from RTC: %s\n", time_string_NTP, time_string_RTC);

    // Update RTC value if it diverges from NTP value
    if (abs(NTP_time - RTC_time_local) >= 5) {
        struct tm* ntp_time_tm = gmtime(&NTP_time);
        set_rtc_time(*ntp_time_tm, true);
        int (*stop)(const struct device* dev) = ((struct ethernet_api*)eth_device->api)->stop;
        stop(eth_device);
    }
}

K_THREAD_DEFINE(net_thread, 2 * 1024, net_function, NULL, NULL, NULL, 10, 0, 0);