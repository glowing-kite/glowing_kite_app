#include <drivers/counter.h>
#include <time.h>

/**
 * @brief Struct that holds hour and minute of alarm. Callback will be called once RTC reaches the alarm time.
 * 
 */
typedef struct stm32_alarm {
    uint32_t hour;
    uint32_t minute;
    counter_alarm_callback_t callback;
} stm32_alarm;

/**
 * @brief Reads the time and date from RTC and returns it as a struct tm. Timezone is the local timezone (MEZ/MESZ)
 * 
 * @return struct tm 
 */
struct tm get_rtc_tm_time(void);

/**
 * @brief Reads the time and date from RTC, adjusts is to UTC timezone returns it as a time_t.
 * 
 * @return time_t 
 */
time_t get_rtc_time_t_utc(void);

/**
 * @brief Sets the RTC to time and date of set_time. If asjust_saylight_saving is true, set_time is expected to be in UTC
 *        and conversion to MEZ/MESZ is carried out.
 * 
 * @param set_time Time to set the RTC to.
 * @param adjust_daylight_saving Set to true if time is in UTC.
 * @return int returns 0.
 */
int set_rtc_time(struct tm set_time, bool adjust_daylight_saving);

/**
 * @brief Returns alarm object for ALARMA.
 * 
 * @param alarm Pointer where result is written to.
 * @return int returns 0.
 */
int get_rtc_alarm(stm32_alarm* alarm);

/**
 * @brief Activate RTC ALARMA
 * 
 * @param alarm Time when to trigger alarm.
 * @return int returns 0.
 */
int set_rtc_alarm(stm32_alarm alarm);

/**
 * @brief Checks if daylightsaving happened while the machine was off and adjusts the RTC time to MEZ or MESZ respectively.
 *        Also schedules an alarm for the next daylightsaving event.
 * 
 * @return uint32_t returns 0
 */
uint32_t handle_daylight_saving(void);

/**
 * @brief Save data in backup register of RTC.
 * 
 * @param addr Address of the register. Can be 0x1 or 0x2.
 * @param data Data to be saved.
 * @return uint32_t returns 0xFFFFFFF on invalid address, 0x0 otherwise.
 */
uint32_t save_in_bkp(uint32_t addr, uint32_t data);

/**
 * @brief Writes saved data in backup register to provided pointer.
 * 
 * @param addr Address of the register. Can be 0x1 or 0x2.
 * @param data Pointer to result location.
 * @return uint32_t returns 0xFFFFFFF on invalid address, 0x0 otherwise.
 */
uint32_t get_from_bkp(uint32_t addr, uint32_t* data);