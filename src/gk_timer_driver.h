#include <stdint.h>
#include <stm32_ll_tim.h>


typedef void (*gk_timer_callback_t)(void);

/**
 * @brief Register callback that will be called on timer match interrupt.
 * 
 * @param callback The function to call back.
 */
void register_gk_timer_callback(gk_timer_callback_t callback);

/**
 * @brief Disable the timer.
 * 
 */
void disable_gk_timer(void);

/**
 * @brief Initialize the timer
 * 
 * @param init Struct that holds all possible parameters.
 * @return int returns 0 on success.
 */
int init_timer(LL_TIM_InitTypeDef init);