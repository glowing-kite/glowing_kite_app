#include <kernel.h>
#include <zephyr.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <lvgl.h>

#include <device.h>
#include <drivers/display.h>
#include <drivers/gpio.h>
#include <drivers/pwm.h>

#include "fade.h"
#include "gk_rtc.h"
#include "gk_timer_driver.h"
#include "hw_button_handler.h"
#include "main.h"

// GPIO pin that controls the background lighting of the display
const struct device* background_led_port;

/**
 * All these variables need to be global since they are modified in the button press functions.
 */

// These variables are responsible for the time string that is used in the set alarm screen.
K_MUTEX_DEFINE(set_alarm_time_string_mut);
static char set_alarm_time_string[] = "#00ff00 %02d#:#000000 %02d#\0";
static lv_obj_t* set_alarm_time_label = 0;
static int8_t set_alarm_time_position = 0;
static uint8_t set_alarm_time_values[3] = { 11, 30, 6 };

// This variable is read in the alarm callback function. If it is 0 fading/tone is not started.
static uint8_t alarm_active = 1;
static lv_obj_t* alarm_active_label;

K_MUTEX_DEFINE(set_light_settings_mut);
// time leds will light up before acutal alarm
static uint32_t set_light_start_light_settings = 15;
// percentage of maximum led brightness
static uint32_t set_light_max_brightness_settings = 100;
// These are used to restore the actual light settings if the user switches the screen without pressing the save button
static uint32_t set_light_start_light_saved = 15;
static uint32_t set_light_max_brightness_saved = 500000;

// These variables correspond to the light settings screen strings
static uint8_t set_light_setting_position = 0;
static char set_light_settings_string[] = "start light: #00ff00 %d# min\nmax brightness: #000000 %d#%%";
static lv_obj_t* set_light_start_light_label;

// Address of RTC backup register
static const uint32_t start_light_bkp_addr = 0x1;
static const uint32_t max_brightness_bkp_addr = 0x2;

// Saves the current mode/screen
static alarm_clock_mode* current_mode;
// Saves all modes/screens
static alarm_clock_mode* mode_array[3];

// Function that is called by RTC subsytem when an alarm is triggered
static void alarm_func(const struct device* dev,
    uint8_t id,
    uint32_t syncclock,
    void* ud)
{
    if (alarm_active) {
        fade_pwm(set_light_max_brightness_saved, set_light_start_light_saved * 60);
    }
}

void set_set_alarm_time_label(void)
{
    lv_label_set_text_fmt(set_alarm_time_label, set_alarm_time_string, set_alarm_time_values[0], set_alarm_time_values[1]);
}

/**
 * @brief The button callback function submits the actual button function to a work queue
 * to process the button function in a non interrupt context
 * 
 * @param button_nr Nr of the button that was pressed 
 */
void button_callback(uint32_t button_nr)
{
    k_work_submit(&current_mode->button_work[button_nr]);
}

uint8_t time_to_string(char* time_string, char* date_string)
{
    char* local_time_string_template = "%02u:%02u:%02u\0";
    char* local_date_string_template = "%s %0u %s %u\0";

    char* month_strings[12] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dez" };
    char* weekday_strings[7] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

    struct tm now = get_rtc_tm_time();

    sprintf(time_string, local_time_string_template, now.tm_hour, now.tm_min, now.tm_sec);
    sprintf(date_string, local_date_string_template, weekday_strings[now.tm_wday], now.tm_mday, month_strings[now.tm_mon], 1900 + now.tm_year);
    return 0;
}

void time_button_0(struct k_work* item)
{
    current_mode = mode_array[LIGHT_MODE];
    lv_scr_load(mode_array[LIGHT_MODE]->screen);
}

void time_button_2(struct k_work* item)
{
    if (alarm_active) {
        alarm_active = 0;
        lv_label_set_text(alarm_active_label, "alarm not\nactive");
    } else {
        alarm_active = 1;
        lv_label_set_text(alarm_active_label, "alarm \nactive");
    }
}

void time_button_3(struct k_work* item)
{
    stop_fade();
}

/**
 * @brief To switch the display on after it was switched off, all button functions are replaced with the turn on function.
 */
void time_button_1(struct k_work* item);
void time_button_switch_display_on(struct k_work* item)
{
    k_work_init(&mode_array[TIME_MODE]->button_work[0], &time_button_0);
    k_work_init(&mode_array[TIME_MODE]->button_work[1], &time_button_1);
    k_work_init(&mode_array[TIME_MODE]->button_work[2], &time_button_2);
    k_work_init(&mode_array[TIME_MODE]->button_work[3], &time_button_3);
    gpio_pin_set_raw(background_led_port, LED_BACKGROUND_PIN, 1);
}

void time_button_1(struct k_work* item)
{
    k_work_init(&mode_array[TIME_MODE]->button_work[0], &time_button_switch_display_on);
    k_work_init(&mode_array[TIME_MODE]->button_work[1], &time_button_switch_display_on);
    k_work_init(&mode_array[TIME_MODE]->button_work[2], &time_button_switch_display_on);
    k_work_init(&mode_array[TIME_MODE]->button_work[3], &time_button_switch_display_on);
    gpio_pin_set_raw(background_led_port, LED_BACKGROUND_PIN, 0);
    return;
}

void light_button_0(struct k_work* item)
{
    set_light_start_light_settings = set_light_start_light_saved;
    set_light_max_brightness_settings = 50000000.0 / set_light_max_brightness_saved;

    current_mode = mode_array[TIME_MODE];
    lv_scr_load(mode_array[TIME_MODE]->screen);
}

void light_button_1(struct k_work* item)
{
    k_mutex_lock(&set_light_settings_mut, K_FOREVER);
    switch (set_light_setting_position) {
    case 0: {
        set_light_start_light_settings = (set_light_start_light_settings + 1) % 60;
        break;
    };
    case 1: {
        set_light_max_brightness_settings = (set_light_max_brightness_settings + 1) % 101;
        break;
    };
    }
    lv_label_set_text_fmt(set_light_start_light_label, set_light_settings_string, set_light_start_light_settings, set_light_max_brightness_settings);
    k_mutex_unlock(&set_light_settings_mut);
}

void light_button_2(struct k_work* item)
{
    int8_t next_pos = (set_light_setting_position + 1) % 2;
    k_mutex_lock(&set_light_settings_mut, K_FOREVER);
    switch (set_light_setting_position) {
    case 0:
        set_light_settings_string[16] = '0';
        set_light_settings_string[17] = '0';
        break;
    case 1:
        set_light_settings_string[48] = '0';
        set_light_settings_string[49] = '0';
        break;
    }

    switch (next_pos) {
    case 0:
        set_light_settings_string[16] = 'f';
        set_light_settings_string[17] = 'f';
        break;
    case 1:
        set_light_settings_string[48] = 'f';
        set_light_settings_string[49] = 'f';
        break;
    }
    set_set_alarm_time_label();
    lv_label_set_text_fmt(set_light_start_light_label, set_light_settings_string, set_light_start_light_settings, set_light_max_brightness_settings);
    set_light_setting_position = next_pos;
    k_mutex_unlock(&set_light_settings_mut);
}
void light_button_3(struct k_work* item)
{
    // 50000000 is max_value (500000) * 100, so it can be devided by a number between 0 - 100 to get percentage
    set_light_max_brightness_saved = (uint32_t)(50000000.0 / set_light_max_brightness_settings);
    set_light_start_light_saved = set_light_start_light_settings;

    current_mode = mode_array[ALARM_MODE];
    lv_scr_load(mode_array[ALARM_MODE]->screen);
    return;
}

void set_alarm_button_0(struct k_work* item)
{
    stm32_alarm tmp_alarm;
    get_rtc_alarm(&tmp_alarm);

    if (tmp_alarm.minute + set_light_start_light_saved > 60) {
        set_alarm_time_values[0] = (tmp_alarm.hour + 1) % 24;
        set_alarm_time_values[1] = (tmp_alarm.minute + set_light_start_light_saved) % 60;
    } else {
        set_alarm_time_values[0] = tmp_alarm.hour;
        set_alarm_time_values[1] = tmp_alarm.minute + set_light_start_light_saved;
    }

    set_set_alarm_time_label();
    current_mode = mode_array[TIME_MODE];
    lv_scr_load(mode_array[TIME_MODE]->screen);
}

void set_alarm_button_1(struct k_work* item)
{
    k_mutex_lock(&set_alarm_time_string_mut, K_FOREVER);
    switch (set_alarm_time_position) {
    case 0: { // hour
        set_alarm_time_values[0] = (set_alarm_time_values[0] + 1) % 24;
        break;
    };
    case 1: { // minute
        set_alarm_time_values[1] = (set_alarm_time_values[1] + 1) % 60;
        break;
    };
    case 2: { // day
        set_alarm_time_values[2] = (set_alarm_time_values[2] + 1) % 8;
        break;
    };
    }
    set_set_alarm_time_label();
    k_mutex_unlock(&set_alarm_time_string_mut);
}

void set_alarm_button_2(struct k_work* item)
{

    int8_t next_pos = (set_alarm_time_position + 1) % 2;
    k_mutex_lock(&set_alarm_time_string_mut, K_FOREVER);
    switch (set_alarm_time_position) {
    case 0:
        set_alarm_time_string[3] = '0';
        set_alarm_time_string[4] = '0';
        break;
    case 1:
        set_alarm_time_string[17] = '0';
        set_alarm_time_string[18] = '0';
        break;
    }
    switch (next_pos) {
    case 0:
        set_alarm_time_string[3] = 'f';
        set_alarm_time_string[4] = 'f';
        break;
    case 1:
        set_alarm_time_string[17] = 'f';
        set_alarm_time_string[18] = 'f';
        break;
    }
    set_set_alarm_time_label();
    set_alarm_time_position = next_pos;
    k_mutex_unlock(&set_alarm_time_string_mut);
}

void set_alarm_button_3(struct k_work* item)
{
    stm32_alarm alarm;
    if (set_light_start_light_saved > set_alarm_time_values[1]) {
        alarm.hour = (set_alarm_time_values[0] - 1) % 24;
        alarm.minute = set_alarm_time_values[1] + 60 - set_light_start_light_saved;
    } else {
        alarm.hour = set_alarm_time_values[0];
        alarm.minute = set_alarm_time_values[1] - set_light_start_light_saved;
    }

    alarm.callback = &alarm_func;

    set_rtc_alarm(alarm);

    save_in_bkp(max_brightness_bkp_addr, set_light_max_brightness_settings);
    save_in_bkp(start_light_bkp_addr, set_light_start_light_saved);

    printf("\nset alarm at: %u:%u \n", alarm.hour, alarm.minute);
    alarm_active = 1;
    lv_label_set_text(alarm_active_label, "alarm \nactive");
    current_mode = mode_array[TIME_MODE];
    lv_scr_load(mode_array[TIME_MODE]->screen);
}

void init_func(void)
{
    // Initialize display hardware
    const struct device* display_dev;
    display_dev = device_get_binding(CONFIG_LVGL_DISPLAY_DEV_NAME);
    if (display_dev == NULL) {
        printf("Display device not found\n");
        return;
    }
    background_led_port = device_get_binding(LED_BACKGROUND_PORT_LABEL);
    if (background_led_port == NULL) {
        printk("Error: didn't find %s device\n", LED_BACKGROUND_PORT_LABEL);
        return;
    }
    int ret = gpio_pin_configure(background_led_port, LED_BACKGROUND_PIN, LED_BACKGROUND_CONFIG);
    if (ret != 0) {
        printk("Error %d: failed to configure %s pin %d\n",
            ret, LED_BACKGROUND_PORT_LABEL, LED_BACKGROUND_PIN);
        return;
    }
    // Switch on background LED
    gpio_pin_set_raw(background_led_port, LED_BACKGROUND_PIN, 1);

    handle_daylight_saving();

    // Get the light settings from backup registers to restore them for the GUI
    uint32_t tmp_backup_val = 0;
    get_from_bkp(start_light_bkp_addr, &tmp_backup_val);
    if (tmp_backup_val) {
        set_light_start_light_settings = tmp_backup_val;
        set_light_start_light_saved = set_light_start_light_settings;
    } else {
        printf("start light no val\n");
    }

    get_from_bkp(max_brightness_bkp_addr, &tmp_backup_val);
    if (tmp_backup_val) {
        set_light_max_brightness_settings = tmp_backup_val;
        set_light_max_brightness_saved = (uint32_t)(50000000.0 / set_light_max_brightness_settings);
    } else {
        printf("max_brighness no val\n");
    }

    // Create all Screens/GUI objects
    
    lv_obj_t* time_screen = lv_obj_create(NULL, NULL);
    lv_style_t style_label_time;
    lv_style_init(&style_label_time);
    lv_style_set_text_font(&style_label_time, LV_STATE_DEFAULT, &lv_font_montserrat_38);
    lv_style_set_text_color(&style_label_time, LV_STATE_DEFAULT, lv_color_make(0x0, 0x0, 0x0));

    char time_time_string[20] = "00:00:00\0\0";
    lv_obj_t* time_label = lv_label_create(time_screen, NULL);
    lv_obj_reset_style_list(time_label, LV_LABEL_PART_MAIN);
    lv_obj_add_style(time_label, LV_LABEL_PART_MAIN, &style_label_time);
    lv_label_set_text(time_label, time_time_string);
    lv_obj_align(time_label, NULL, LV_ALIGN_CENTER, 0, -20);

    lv_style_t style_label_date;
    lv_style_init(&style_label_date);
    lv_style_set_text_font(&style_label_date, LV_STATE_DEFAULT, &lv_font_montserrat_24);
    lv_style_set_text_color(&style_label_date, LV_STATE_DEFAULT, lv_color_make(0x0, 0x0, 0x0));

    char time_date_string[16] = "Sun 01 Jan 2021\0";
    lv_obj_t* date_label = lv_label_create(time_screen, NULL);
    lv_obj_reset_style_list(date_label, LV_LABEL_PART_MAIN);
    lv_obj_add_style(date_label, LV_LABEL_PART_MAIN, &style_label_date);
    lv_label_set_text(date_label, time_date_string);
    lv_obj_align(date_label, NULL, LV_ALIGN_CENTER, 0, 10);

    lv_style_t style_label_alarm_active;
    lv_style_init(&style_label_alarm_active);
    lv_style_set_text_font(&style_label_alarm_active, LV_STATE_DEFAULT, &lv_font_montserrat_12);
    lv_style_set_text_color(&style_label_alarm_active, LV_STATE_DEFAULT, lv_color_make(0x0, 0x0, 0x0));

    alarm_active_label = lv_label_create(time_screen, NULL);
    lv_obj_reset_style_list(alarm_active_label, LV_LABEL_PART_MAIN);
    lv_obj_add_style(alarm_active_label, LV_LABEL_PART_MAIN, &style_label_alarm_active);
    lv_label_set_text(alarm_active_label, "alarm not\nactive");
    lv_obj_align(alarm_active_label, NULL, LV_ALIGN_IN_TOP_RIGHT, -10, 10);
    lv_label_set_text(alarm_active_label, "alarm \nactive");

    lv_obj_t* btnm_time;
    const char* btnm_map_time[] = { "setup\nalarm", "disp\noff", "toggle\nalarm", "stop\nalarm", "" };
    btnm_time = lv_btnmatrix_create(time_screen, NULL);
    lv_btnmatrix_set_map(btnm_time, btnm_map_time);
    lv_obj_set_height(btnm_time, 60);
    lv_obj_set_width(btnm_time, 320);
    lv_obj_align(btnm_time, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);
    lv_obj_set_event_cb(btnm_time, NULL);

    lv_obj_t* set_alarm_screen;
    set_alarm_screen = lv_obj_create(NULL, NULL);

    const char* set_alarm_btnm_map_bottom[] = { "reset", "+", "-->", "save\nalarm", "" };
    lv_obj_t* set_alarm_btnm_bottom;
    set_alarm_btnm_bottom = lv_btnmatrix_create(set_alarm_screen, NULL);
    lv_btnmatrix_set_map(set_alarm_btnm_bottom, set_alarm_btnm_map_bottom);
    lv_obj_set_height(set_alarm_btnm_bottom, 60);
    lv_obj_set_width(set_alarm_btnm_bottom, 320);
    lv_obj_align(set_alarm_btnm_bottom, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);
    lv_obj_set_event_cb(set_alarm_btnm_bottom, NULL);

    lv_style_t set_alarm_style_label_top;
    lv_style_init(&set_alarm_style_label_top);
    lv_style_set_text_font(&set_alarm_style_label_top, LV_STATE_DEFAULT, &lv_font_montserrat_24);
    lv_style_set_text_color(&set_alarm_style_label_top, LV_STATE_DEFAULT, lv_color_make(0x0, 0x0, 0x0));

    lv_obj_t* set_alarm_lable_top = lv_label_create(set_alarm_screen, NULL);
    lv_obj_reset_style_list(set_alarm_lable_top, LV_LABEL_PART_MAIN);
    lv_obj_add_style(set_alarm_lable_top, LV_LABEL_PART_MAIN, &set_alarm_style_label_top);
    lv_label_set_text(set_alarm_lable_top, "Set Alarm");
    lv_obj_align(set_alarm_lable_top, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);

    lv_style_t set_alarm_style_label_time;
    lv_style_init(&set_alarm_style_label_time);
    lv_style_set_text_font(&set_alarm_style_label_time, LV_STATE_DEFAULT, &lv_font_montserrat_48);
    lv_style_set_text_color(&set_alarm_style_label_time, LV_STATE_DEFAULT, lv_color_make(0x0, 0x0, 0x0));

    set_alarm_time_label = lv_label_create(set_alarm_screen, NULL);
    lv_obj_reset_style_list(set_alarm_time_label, LV_LABEL_PART_MAIN);
    lv_obj_add_style(set_alarm_time_label, LV_LABEL_PART_MAIN, &set_alarm_style_label_time);
    lv_label_set_recolor(set_alarm_time_label, true);
    lv_label_set_text(set_alarm_time_label, set_alarm_time_string);

    // restore old alarm
    stm32_alarm tmp_alarm;
    get_rtc_alarm(&tmp_alarm);
    if (tmp_alarm.minute + set_light_start_light_saved >= 60) {
        set_alarm_time_values[0] = (tmp_alarm.hour + 1) % 24;
        set_alarm_time_values[1] = (tmp_alarm.minute + set_light_start_light_saved) % 60;
    } else {
        set_alarm_time_values[0] = tmp_alarm.hour;
        set_alarm_time_values[1] = tmp_alarm.minute + set_light_start_light_saved;
    }
    tmp_alarm.callback = &alarm_func;
    set_rtc_alarm(tmp_alarm);

    set_set_alarm_time_label();
    lv_obj_align(set_alarm_time_label, NULL, LV_ALIGN_CENTER, 0, -20);

    lv_obj_t* set_light_screen = lv_obj_create(NULL, NULL);

    lv_style_t light_style_label_top;
    lv_style_init(&light_style_label_top);
    lv_style_set_text_font(&light_style_label_top, LV_STATE_DEFAULT, &lv_font_montserrat_24);
    lv_style_set_text_color(&light_style_label_top, LV_STATE_DEFAULT, lv_color_make(0x0, 0x0, 0x0));

    lv_obj_t* light_lable_top = lv_label_create(set_light_screen, NULL);
    lv_obj_reset_style_list(light_lable_top, LV_LABEL_PART_MAIN);
    lv_obj_add_style(light_lable_top, LV_LABEL_PART_MAIN, &light_style_label_top);
    lv_label_set_text(light_lable_top, "Light Settings");
    lv_obj_align(light_lable_top, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);

    const char* set_light_btnm_map_bottom[] = { "back", "+", "-->", "save\nlight", "" };
    lv_obj_t* set_light_btnm_bottom;
    set_light_btnm_bottom = lv_btnmatrix_create(set_light_screen, NULL);
    lv_btnmatrix_set_map(set_light_btnm_bottom, set_light_btnm_map_bottom);
    lv_obj_set_height(set_light_btnm_bottom, 60);
    lv_obj_set_width(set_light_btnm_bottom, 320);
    lv_obj_align(set_light_btnm_bottom, NULL, LV_ALIGN_IN_BOTTOM_LEFT, 0, 0);
    lv_obj_set_event_cb(set_light_btnm_bottom, NULL);

    lv_style_t set_light_style_start_light;
    lv_style_init(&set_light_style_start_light);
    lv_style_set_text_font(&set_light_style_start_light, LV_STATE_DEFAULT, &lv_font_montserrat_24);
    lv_style_set_text_color(&set_light_style_start_light, LV_STATE_DEFAULT, lv_color_make(0x0, 0x0, 0x0));

    set_light_start_light_label = lv_label_create(set_light_screen, NULL);
    lv_obj_reset_style_list(set_light_start_light_label, LV_LABEL_PART_MAIN);
    lv_obj_add_style(set_light_start_light_label, LV_LABEL_PART_MAIN, &set_light_style_start_light);
    lv_label_set_recolor(set_light_start_light_label, true);
    lv_label_set_text_fmt(set_light_start_light_label, set_light_settings_string, set_light_start_light_settings, set_light_max_brightness_settings);
    lv_obj_align(set_light_start_light_label, NULL, LV_ALIGN_IN_TOP_LEFT, 10, 40);

    alarm_clock_mode time_mode;
    alarm_clock_mode set_alarm_mode;
    alarm_clock_mode set_light_mode;

    time_mode.screen = time_screen;
    k_work_init(&time_mode.button_work[0], &time_button_0);
    k_work_init(&time_mode.button_work[1], &time_button_1);
    k_work_init(&time_mode.button_work[2], &time_button_2);
    k_work_init(&time_mode.button_work[3], &time_button_3);

    set_alarm_mode.screen = set_alarm_screen;
    k_work_init(&set_alarm_mode.button_work[0], &set_alarm_button_0);
    k_work_init(&set_alarm_mode.button_work[1], &set_alarm_button_1);
    k_work_init(&set_alarm_mode.button_work[2], &set_alarm_button_2);
    k_work_init(&set_alarm_mode.button_work[3], &set_alarm_button_3);

    set_light_mode.screen = set_light_screen;
    k_work_init(&set_light_mode.button_work[0], &light_button_0);
    k_work_init(&set_light_mode.button_work[1], &light_button_1);
    k_work_init(&set_light_mode.button_work[2], &light_button_2);
    k_work_init(&set_light_mode.button_work[3], &light_button_3);

    mode_array[TIME_MODE] = &time_mode;
    mode_array[ALARM_MODE] = &set_alarm_mode;
    mode_array[LIGHT_MODE] = &set_light_mode;

    current_mode = &time_mode;

    init_buttons(button_callback);

    lv_scr_load(time_screen);
    lv_task_handler();
    display_blanking_off(display_dev);
    uint32_t counter = 0;
    while (1) {
        if (counter % 100 == 0) {
            time_to_string(time_time_string, time_date_string);
            lv_label_set_text(time_label, time_time_string);
            lv_label_set_text(date_label, time_date_string);
        }
        ++counter;
        lv_task_handler();
        k_sleep(K_MSEC(10));
    }
}

K_THREAD_DEFINE(display_thread, 4 * STACKSIZE, init_func, NULL, NULL, NULL, 0, 0, 0);
