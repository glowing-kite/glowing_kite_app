
#define TIME_MODE 0
#define ALARM_MODE 1
#define LIGHT_MODE 2

#define STACKSIZE 1024

#define LED_BACKGROUND_PORT_LABEL DT_GPIO_LABEL_BY_IDX(DT_NODELABEL(led_background), gpios, 0)
#define LED_BACKGROUND_PIN DT_GPIO_PIN(DT_NODELABEL(led_background), gpios)
#define LED_BACKGROUND_CONFIG (GPIO_OUTPUT | GPIO_PUSH_PULL | DT_GPIO_FLAGS(DT_NODELABEL(led_background), gpios))

typedef struct alarm_clock_mode {
    lv_obj_t* screen;
    struct k_work button_work[6];
} alarm_clock_mode;