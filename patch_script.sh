SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PATCH_FOLDER_PATH=$SCRIPT_PATH"/patches"
ZEPHYR_PATH=$SCRIPT_PATH"/../zephyr"

cd $ZEPHYR_PATH
git apply $PATCH_FOLDER_PATH"/arp_fix.patch"
git apply $PATCH_FOLDER_PATH"/counter_driver.patch"
git apply $PATCH_FOLDER_PATH"/enable_sleep_debug.patch"
git apply $PATCH_FOLDER_PATH"/ethernet_driver.patch"
git apply $PATCH_FOLDER_PATH"/ethernet_dts_bindings.patch"