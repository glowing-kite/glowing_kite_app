# Glowing Kite Software
This repo contains the Zephyr app for the glowing kite light alarm clock. The alarm clock consists of a custom [PCB](https://gitlab.com/glowing-kite/pcb), a 3D printable [case](https://gitlab.com/glowing-kite/case), and this code. Its purpose is to make waking up easy by turning on the LEDs sometime before the actual alarm time. The LED intensity increases until the programmed alarm time is reached then an alarm sound is started. The initial time is fetched via NTP and adjusted to MEZ/MESZ. This is done once after reset.

Feature summary:
- Gets initial time over ethernet
- Time displayed is MEZ/MESZ
- Automatic daylight savings adjustment
- Programmable LED start time before actual alarm sound
- Maximum LED intensity adjustable
- RTC saves data/alarm and light settings even if the power plug is not connected (battery)
- UART Pins
- SWD for programming


## Picture
<img src="./docs/glowing-kite.jpg" alt="drawing" width="250"/>

## How to build
### Get the version of Zehpyr I used
```
west init -m https://github.com/zephyrproject-rtos/zephyr --mr v2.5.0-rc2 zephyr_workspace
```
### Update
```
cd zephyr_workspace
west update
```
### Get Glowing Kite app
```
git clone 
```
### Apply Patches
```
./glowing_kite_app/patch_script.sh
```
### Build
```
west build -b glowing_kite_board glowing_kite_app
```
### Flash
```
west flash
```
## The Patchfiles
I had to do some alterations of the Zephyr kernel
### arp_fix
The enc28j60 (ethernet chip) sometimes loses the first packet one tries to send. One solution is to wait some time, after that the first packet is not lost. I found out that Zephyrs ARP implementation deletes the return mac address for a resolution request if it's there is no answer the first time it is sent. I changed that. Not sure if that's supposed to happen I might ask the Zephyr people about this and update the patch.

### counter_driver
First, I added handling for the second RTC alarm interrupt. Initially, the driver only supported interrupts from ALARMA. Second I added a check in the init function that finds out if the backup domain of the RTC is in use. If that's the case, I don't reset it. Also, don't switch off ALARMA if the interrupt occurs. This makes it so that the alarm doesn't need to be reprogrammed.

### enable_sleep_debug
Enable debugging in sleep mode to make it possible to always reprogram.

### ethernet_driver
I added the possibility to turn the enc28j60 off/on by setting a bit in a register to save power. I also added handling of the reset pin to do a hardware reset.

### ethernet_dts_bindings
This adds the device tree bindings for the reset pin.
